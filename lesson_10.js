/*
 1. Создать конструктор Student для объекта студента, который принимает имя и кол-во баллов в качестве аргумента. 
    Все объекты созданные этим конструктором должны иметь общий метод show, 
    который работает так же, как в предыдущем домашнем задании.
*/
function Student (name, point) {
  this.name = name;
  this.point = point;
}
 
Student.prototype.show = function () {
  console.log('Студент %s набрал %d баллов', this.name, this.point);
}
 
/*
  2. Создать конструктор StudentList, который в качестве аргумента принимает название группы и массив формата
     studentsAndPoints из задания по массивам. Массив может отсутствовать или быть пустым. Объект, созданный 
     этим конструктором должен обладать всеми функциями массива. А так же иметь метод add, который принимает
     в качестве аргументов имя и кол-во баллов, создает нового студента и добавляет его в список.
*/
StudentList.prototype = Array.prototype;
function StudentList (groupName, students) {
  
  var tmp = this;
  
  if (students != undefined) {
    students.forEach(function (student, index) { 
      if(isNaN(student)) {
        tmp.push(new Student(student, students[index+1]));
      }
    })
  }
  
  this.add = function(name, point) {
    this.push(new Student(name, point));
  }
  
  this.name = groupName;
}
 
/*
  3. Создать список студентов группы «HJ-2» и сохранить его в переменной hj2. Воспользуйтесь массивом studentsAndPoints из файла.
*/
 
var studentsAndPoints = [
  'Алексей Петров', 0,
  'Ирина Овчинникова', 60,
  'Глеб Стукалов', 30,
  'Антон Павлович', 30,
  'Виктория Заровская', 30,
  'Алексей Левенец', 70,
  'Тимур Вамуш', 30,
  'Евгений Прочан', 60,
  'Александр Малов', 0
];
 
var hj2 = new StudentList('HJ-2', studentsAndPoints);

/*
  4. Добавить несколько новых студентов в группу. 
     Имена и баллы придумайте самостоятельно.
*/
hj2.add('Наталья Терентьева', 100);
hj2.add('Кирилл Васильев', 50);
 
/*
  5. Создать группу «HTML-7» без студентов и сохранить его в переменной html7. 
     После чего добавить в группу несколько студентов.
     Имена и баллы придумайте самостоятельно.
*/

var html7 = new StudentList('HTML-l7');
html7.add('Наталья Терентьева', 100);
html7.add('Кирилл Васильев', 50);
html7.add('Супер Марио', 100500);

/*
  6. Добавить спискам студентов метод show, который выводит информацию о группе в следующем виде:
*/
 
StudentList.prototype.show = function () {
  console.log("\nГруппа %s (%s студентов)", this.name, this.length);
  this.forEach( function (student) {
    student.show();
  })
}
 
html7.show();
hj2.show();
 
/*
  7. Перевести одного из студентов из группы «HJ-2» в группу «HTML-7»
*/
 
html7.add(hj2[2].name, hj2[2].point);
hj2.splice(2,1);
 
hj2.show();
html7.show();
 
